# forumscrape-Aug2019

a snapshot set of static webpages, Aug 8 2019, from antixforum.com<br>
to enable offline viewing + searching the forum content

If you are not interested in searching the content, you can just unpack the tar.gz file and view in browser<br>
forumscrape.tar.gz (5.6MB; FYI, expanded size of contents is 37MB)

The following instructions will create an indexed, searchable, recoll database on your local machine.

sudo apt install git<br>
cd ~/Documents<br>
git clone https://gitlab.com/antix-contribs/forumscrape-aug2019<br>
mv forumscrape-aug2019 antixforum<br>
rm -Rf ~/Documents/antixforum/.git<br>
rm -Rf ~/Documents/antixforum/gitimg<br>
rm ~/Documents/antixforum/forumscrape.tar.gz<br>
sudo apt install recoll<br>

At this point, you can browse the offline forum content. The main page will be avialable at:<br>
file:///~/Documents/antixforum/index.html

From terminal prompt, or via Alt+F2 runbox, launch the recoll program:<br>
recoll

and click "Indexing Configuration"<br>
![image](gitimg/r1.png ".")

By default, recoll is preconfigured to index the entire contents of your home directory.<br>
Later, some other day, you might wish to do so... but for now, highlight "~" and click "minus" to deselect that default path.<br>
![image](gitimg/r2.png ".")

Browse to, and select (Choose) the directory containing the set of forum offline files...<br>
![image](gitimg/r3.png ".")

and click "OK"<br>
![image](gitimg/r4.png ".")

then click "Start Indexing Now".<br>
You should expect the indexing operation will take 1-3 minutes to complete.<br>
![image](gitimg/r5.png ".")

When you see this dialog, just click "OK". (It displays a notification, not a warning)<br>
![image](gitimg/r6.png ".")

As soon as the one-time indexing operation has completed, and each time you launch recoll,<br>
the main searach GUI window enables you to query and view the contents of the indexed offline forum pages.<br>
Getting started is simple, but you can//should read the recoll user documentation to discover its full portential.<br>
![image](gitimg/r6a.png ".")

For each matching search result, you can PREVIEW the page using recoll's built-in document viewer<br>
or OPEN WITH web browser or OTHER<br>
(on my machine, "Open" opens the document in a text editor, probably not what you want)<br>
![image](gitimg/r6b.png ".")

This icon leads to the ADVANCED SEARCH dialog (shown in the bottom screenshot, below).<br>
While working with simple html pages, you will seldom (probably never) have need to visit this,<br>
just know that recoll is an extremely powerful search indexer, capable of parsing a wide range of documents.<br>
![image](gitimg/r7.png ".")

![image](gitimg/r8.png ".")
